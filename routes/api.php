<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//convierto url larga a corta
Route::post('url', 'App\Http\Controllers\UrlController@store');

//elimino url corta
Route::delete('eliminar/{url_id}', 'App\Http\Controllers\UrlController@destroy');

//listado de peticiones
Route::get('/listar/estadistica', 'App\Http\Controllers\UrlController@index');

//agrupado de peticiones por cantidad
Route::get('/listar/totalPorUrl', 'App\Http\Controllers\UrlController@traerAgrupadoURL');