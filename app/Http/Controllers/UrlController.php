<?php

namespace App\Http\Controllers;

use App\Models\Peticiones;
use App\Models\Estadistica;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;

class UrlController extends Controller
{
    /**
     * Lista información de URL, fecha y hora de petición
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $peticiones = DB::table('estadisticafechahora')->get();
       
        if ($peticiones->isNotEmpty()) {
                return response()->json([
                    'resultado' =>  $peticiones
                ], 200);
            } else {
                return response()->json([
                    'mensaje' => 'No hay datos guardados.',
                ], 400);
            }
       
    }

    /**
     * Lista las URL, agrupadas por cantidad de peticiones
     *
     * @return \Illuminate\Http\Response
     */
    public function traerAgrupadoURL(Request $request)
    {
        $traerAgrupadoURL = DB::table('contabilizarpeticiones')->get();

        if ($traerAgrupadoURL->isNotEmpty()) {
                return response()->json([
                    'resultado' =>  $traerAgrupadoURL
                ], 200);
            } else {
                return response()->json([
                    'mensaje' => 'No hay datos guardados.',
                ], 400);
            }
    }

    /**
     * Guarda información en base de datos
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validator = \Validator::make($request->all(),
        [ 
            'url' => 'required|url|max:2083',
        ],
        [
            'url.required' => 'Es necesario enviar una URL',
            'url.url' => 'El formato es invalido',
            'url.max' => 'La url supero la cantidad de caracteres máximos, 2083. Intente con una url con menos caracteres.',
        ]);
    
        if ($validator->fails()) {
            return response()->json([
                'mensaje' => 'Error en la petición',
                'validaciones' => $validator->errors()
            ], 400);
        }

        $registro = Peticiones::where('url', $request->url)->first();
        
        if ($registro) {

            //existe URL, guardo log
            $dataEstadistica = Estadistica::create([
                'url_id' => $registro->id,
                'fecha_solicitud' => date('Y-m-d H:i:s'),
            ]);

            return response()->json([
                'mensaje' => 'Ya existe esta URL en nuestro registro.',
                'URL' => $registro->url,
                'URL corta' => $registro->url_corta,
                'ID:' => $registro->id
            ], 200);

        } else {
            
            //no existe URL, guardo petición y log
            try {

                $random = $this->generarRandomString();
                
                $dataPeticiones = Peticiones::create([
                    'url' => $request->url,
                    'url_corta' => env('URL').$random,
                ]);

                $dataEstadistica = Estadistica::create([
                    'url_id' => $dataPeticiones->id,
                    'fecha_solicitud' => date('Y-m-d H:i:s'),
                ]);

                return response()->json([
                    'mensaje' => 'Se ha registrado la URL: '.$request->url,
                    'URL corta' => env('URL').$random,
                    'ID:' => $dataPeticiones->id
                ], 200);

            } catch(\Illuminate\Database\QueryException $e){
                
                $errorCode = $e->errorInfo[1];
                if($errorCode == '1062'){
                    return response()->json([
                        'mensaje' => 'Hubo un problema con el registro de la URL corta. Intente nuevamente.',
                    ], 400);
                }
            }
        }
    }

    /* Genero random string */
    function generarRandomString(){
        $random = Str::random(6);
        return $random;
    }

    /**
     * Redirección de URL
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show($request)
    {
        $url = Peticiones::where('url_corta','=', env('URL').$request)->first();
        
        if(empty($url)){
            return response()->json([
                'mensaje' => 'No se encontro este link.']
            , 400);
        }

        return redirect($url['url']);
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $url_id = $request->url_id;

        $eliminarUsuario = Peticiones::where('id', $url_id)->delete();
        if($eliminarUsuario == 1){
            return response()->json([
                'mensaje' => 'Se ha eliminado la URL con ID: '.$url_id,
            ], 200);
        } else {
            return response()->json([
                'mensaje' => 'No existe una URL con ese ID',
            ], 400);
        }
        
    }

}
