-- acortador.estadisticafechahora source

CREATE OR REPLACE
ALGORITHM = UNDEFINED VIEW `acortador`.`estadisticafechahora` AS
select
    `p`.`id` AS `id`,
    `p`.`url` AS `url`,
    `e`.`fecha_solicitud` AS `fecha_solicitud`
from
    (`acortador`.`estadistica` `e`
join `acortador`.`peticiones` `p` on
    ((`e`.`url_id` = `p`.`id`)));

-- acortador.contabilizarpeticiones source

CREATE OR REPLACE
ALGORITHM = UNDEFINED VIEW `acortador`.`contabilizarpeticiones` AS
select
    count(`p`.`id`) AS `total`,
    `p`.`url` AS `url`
from
    (`acortador`.`estadistica` `e`
join `acortador`.`peticiones` `p` on
    ((`e`.`url_id` = `p`.`id`)))
group by
    `p`.`id`;